using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject vehicle;
    private Vector3 cameraOffset = new Vector3(0, 5, -7);

    void LateUpdate()
    {
        transform.position = vehicle.transform.position + cameraOffset;
    }
}   
