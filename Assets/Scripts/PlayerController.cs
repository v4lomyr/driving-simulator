using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float speed;
    private float turnSpeed;
    private float horizontalInput;
    private float forwardInput;

    void Start()
    {
        speed = 25.0f;
        turnSpeed = 45.0f;
    }

    void Update()
    {
        // Get the player input
        horizontalInput = Input.GetAxis("Horizontal");
        forwardInput = Input.GetAxis("Vertical");

        // Move the vehicle forward / backward
        transform.Translate(Vector3.forward * speed * Time.deltaTime * forwardInput);

        // Rotate the vehicle in y-axis (turn left / right)
        transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime * horizontalInput);
    }
}
